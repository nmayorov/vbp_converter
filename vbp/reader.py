import struct
from . import crc


class MessageReader:
    SYNC_BYTE = 0xAA
    HEADER_FORMAT = '<HHQ'
    HEADER_SIZE = struct.calcsize(HEADER_FORMAT)

    def __init__(self, stream):
        self.stream = stream

    def read_next_message(self):
        message_found = False
        while not message_found:
            sync = self.stream.read(1)
            if not sync:
                break
            if sync[0] != self.SYNC_BYTE:
                continue

            expected_header_crc = self.stream.read(1)
            if not expected_header_crc:
                break

            header = self.stream.read(self.HEADER_SIZE)
            if len(header) < self.HEADER_SIZE:
                break
            if crc.compute_crc_ccit8(header) != expected_header_crc[0]:
                continue

            message_id, message_length, system_time = struct.unpack(self.HEADER_FORMAT,
                                                                    header)
            message = self.stream.read(message_length)
            if len(message) < message_length:
                break

            expected_message_crc = self.stream.read(3)
            if len(expected_message_crc) < 3:
                break

            message_crc = crc.compute_crc_qualcomm24(message)
            if message_crc.to_bytes(3, 'big') != expected_message_crc:
                continue

            message_found = True

        if message_found:
            return message_id, system_time, message

        return None
