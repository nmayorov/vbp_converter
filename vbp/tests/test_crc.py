from vbp import crc


def test_crc():
    assert crc.compute_crc_ccit8("123456789".encode()) == 0xF4
    assert crc.compute_crc_qualcomm24("123456789".encode()) == 0xCDE703
