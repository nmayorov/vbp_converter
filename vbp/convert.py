import struct
from collections import OrderedDict
import numpy as np
import pandas as pd
from .reader import MessageReader


TIME_UNIT = 1e-6


class Bunch(OrderedDict):
    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)

    __setattr__ = OrderedDict.__setitem__
    __delattr__ = OrderedDict.__delitem__

    def __repr__(self):
        if self.keys():
            m = max(map(len, list(self.keys())))
            return '\n'.join(["{}: {}".format(k.rjust(m), type(v)) for k, v in self.items()])
        else:
            return self.__class__.__name__ + "()"

    def __dir__(self):
        return list(self.keys())


def _interpret_global_time(gps_time, leap_seconds, time_flags):
    SECONDS_IN_WEEK = 604800
    GPS_TIME_START = pd.to_datetime("1980-01-06")

    time_valid = time_flags & 1 > 0
    ls_valid = time_flags & 4 > 0

    gps_week = np.full(len(gps_time), -1, dtype=int)
    gps_second = np.full(len(gps_time), np.nan)
    utc = np.full(len(gps_time), "nat", dtype="datetime64[us]")

    gps_time = gps_time * TIME_UNIT

    gps_week[time_valid] = gps_time[time_valid] / SECONDS_IN_WEEK
    gps_second[time_valid] = gps_time[time_valid] - gps_week[time_valid] * SECONDS_IN_WEEK

    utc[ls_valid] = (GPS_TIME_START
                     + pd.to_timedelta(gps_time[ls_valid] - leap_seconds[ls_valid], 's'))

    return gps_week, gps_second, utc


def _replace_global_time(data):
    gps_week, gps_second, utc = _interpret_global_time(data.gps_time, data.leap_seconds,
                                                       data.time_flags)

    result = data.copy()
    result.drop(columns=["gps_time", "leap_seconds", "time_flags"], inplace=True)
    columns = result.columns

    result["gps_week"] = gps_week
    result["gps_second"] = gps_second
    result["utc"] = utc

    return result[["gps_week", "gps_second", "utc"] + columns.to_list()]


def read_messages(path):
    PVA_COLUMNS = [
        "system_time",
        "gps_time", "leap_seconds", "time_flags",
        "filter_status",
        "position_type",
        "latitude",
        "longitude",
        "altitude",
        "velocity_north",
        "velocity_east",
        "velocity_down",
        "roll",
        "pitch",
        "heading",
        "distance"
    ]

    PVA_SD_COLUMNS = [
        "system_time",
        "gps_time", "leap_seconds", "time_flags",
        "position_north_accuracy", "position_east_accuracy", "position_down_accuracy",
        "velocity_north_accuracy", "velocity_east_accuracy", "velocity_down_accuracy",
        "roll_accuracy", "pitch_accuracy", "heading_accuracy",
        "distance_accuracy"
    ]

    DYNAMICS_COLUMNS = [
        "system_time",
        "gps_time", "leap_seconds", "time_flags",
        "angular_rate_x", "angular_rate_y", "angular_rate_z",
        "acceleration_x", "acceleration_y", "acceleration_z"
    ]

    FULL_SOLUTION_COLUMNS = PVA_COLUMNS + PVA_SD_COLUMNS[4:] + DYNAMICS_COLUMNS[4:]

    stream = open(path, "rb")
    reader = MessageReader(stream)

    pva = []
    pva_sd = []
    dynamics = []
    full_solution = []

    while True:
        message_info = reader.read_next_message()
        if message_info is None:
            break
        message_id, system_time, message = message_info

        system_time *= TIME_UNIT
        if message_id == 3000:
            pva.append([system_time])
            pva[-1].extend(struct.unpack('<QHHHHddffffffff', message))
        elif message_id == 3001:
            pva_sd.append([system_time])
            pva_sd[-1].extend(struct.unpack("<QHHffffffffff", message))
        elif message_id == 3002:
            dynamics.append([system_time])
            dynamics[-1].extend(struct.unpack("<QHHffffff", message))
        elif message_id == 3003:
            full_solution.append([system_time])
            full_solution[-1].extend(struct.unpack('<QHHHHddffffffffffffffffffffffff', message))

    stream.close()

    pva = pd.DataFrame(pva, columns=PVA_COLUMNS)
    pva = pva.set_index("system_time")
    pva = _replace_global_time(pva)

    pva_sd = pd.DataFrame(pva_sd, columns=PVA_SD_COLUMNS)
    pva_sd = pva_sd.set_index("system_time")
    pva_sd = _replace_global_time(pva_sd)

    dynamics = pd.DataFrame(dynamics, columns=DYNAMICS_COLUMNS)
    dynamics = dynamics.set_index("system_time")
    dynamics = _replace_global_time(dynamics)

    full_solution = pd.DataFrame(full_solution, columns=FULL_SOLUTION_COLUMNS)
    full_solution = full_solution.set_index('system_time')
    full_solution = _replace_global_time(full_solution)

    result = Bunch()
    result.pva = pva
    result.pva_sd = pva_sd
    result.dynamics = dynamics
    result.full_solution = full_solution

    return result
