from distutils.core import setup

setup_options = dict(
    name="vbp",
    version="0.10.0",
    description="Converter of vbp logs",
    maintainer="Nikolay Mayorov",
    maintainer_email="nikolay.mayorov@zoho.com",
    license="MIT",
    packages=["vbp"],
    scripts=["scripts/vbp_to_csv.py"],
    classifiers=[
        "Programming Language :: Python :: 3"
    ],
    requires=["numpy", "pandas"]
)


setup(**setup_options)
