#!/usr/bin/env python


"""Convert vbp log into several csv files."""
import argparse
import os
from vbp.convert import read_messages


FORMATS = {
    "system_time": "{:.6f}",
    "gps_second": "{:.6f}",
    "latitude": "{:.9f}",
    "longitude": "{:.9f}",
    "altitude": "{:.3f}",
    "velocity_north": "{:.3f}",
    "velocity_east": "{:.3f}",
    "velocity_down": "{:.3f}",
    "roll": "{:.2f}",
    "pitch": "{:.2f}",
    "heading": "{:.2f}",
    "distance": "{:.3f}",
    "position_north_accuracy": "{:.3f}",
    "position_east_accuracy": "{:.3f}",
    "position_down_accuracy": "{:.3f}",
    "velocity_north_accuracy": "{:.3f}",
    "velocity_east_accuracy": "{:.3f}",
    "velocity_down_accuracy": "{:.3f}",
    "roll_accuracy": "{:.2f}",
    "pitch_accuracy": "{:.2f}",
    "heading_accuracy": "{:.2f}",
    "distance_accuracy": "{:.3f}",
    "angular_rate_x": "{:.3f}",
    "angular_rate_y": "{:.3f}",
    "angular_rate_z": "{:.3f}",
    "acceleration_x": "{:.3f}",
    "acceleration_y": "{:.3f}",
    "acceleration_z": "{:.3f}",
}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("path", help="Path to vbp log")
    args = parser.parse_args()

    data_frames = read_messages(args.path)

    output_dir = os.path.splitext(args.path)[0]
    os.makedirs(output_dir, exist_ok=True)

    for name, df in data_frames.items():
        df.index = df.index.map(lambda x: FORMATS["system_time"].format(x))
        for column in df:
            if column in FORMATS:
                fmt = FORMATS[column]
                df[column] = df[column].map(lambda x: fmt.format(x))

        if not df.empty:
            df.to_csv(os.path.join(output_dir, "{}.csv".format(name)))
